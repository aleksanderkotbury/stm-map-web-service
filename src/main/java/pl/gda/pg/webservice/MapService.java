package pl.gda.pg.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.gda.pg.eti.stm.map.*;
import pl.gda.pg.map.MapCoordinates;
import pl.gda.pg.map.MapProvider;

import java.awt.*;
import java.math.BigDecimal;

import static pl.gda.pg.webservice.MapNamespace.NAMESPACE;

@Endpoint
public class MapService {

    private final MapProvider mapProvider;

    @Autowired
    public MapService(MapProvider mapProvider) {
        this.mapProvider = mapProvider;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "getMapRequest")
    @ResponsePayload
    public GetMapResponse getMap(@RequestPayload GetMapRequest request) {
        GetMapResponse response = new GetMapResponse();
        Image mapFragment = mapProvider.getMapFragment(request.getLeftTopCorner(), request.getRightDownCorner());
        response.setMapBase64(mapFragment);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "getMapThumbnailRequest")
    @ResponsePayload
    public GetMapThumbnailResponse getMapThumbnail(@RequestPayload GetMapThumbnailRequest request) {
        GetMapThumbnailResponse response = new GetMapThumbnailResponse();
        response.setMapBase64(mapProvider.getImageThumbnail());
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "getMapCoordinatesBoundariesRequest")
    @ResponsePayload
    public GetMapCoordinatesBoundariesResponse getMapBoundaries(@RequestPayload GetMapCoordinatesBoundariesRequest request) {
        GetMapCoordinatesBoundariesResponse response = new GetMapCoordinatesBoundariesResponse();
        response.setMaxAttitude(BigDecimal.valueOf(MapCoordinates.MAX_ATTITUDE));
        response.setMinAttitude(BigDecimal.valueOf(MapCoordinates.MIN_ATTITUDE));
        response.setMaxLongitude(BigDecimal.valueOf(MapCoordinates.MAX_LONGITUDE));
        response.setMinLongitude(BigDecimal.valueOf(MapCoordinates.MIN_LONGITUDE));
        return response;
    }
}
