package pl.gda.pg.webservice;

public class MapNamespace {

    public static final String NAMESPACE = "http://eti.pg.gda.pl/stm/map";

    private MapNamespace() {
    }
}
