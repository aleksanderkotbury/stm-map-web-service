package pl.gda.pg.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.gda.pg.eti.stm.map.Coordinate;
import pl.gda.pg.map.MapCoordinates;
import pl.gda.pg.map.MapProvider;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

@RestController
public class MapResource {

    private final MapProvider mapProvider;

    @Autowired
    public MapResource(MapProvider mapProvider) {
        this.mapProvider = mapProvider;
    }

    @GetMapping(path = "/maps/thumbnail", produces = MediaType.IMAGE_PNG_VALUE)
    public void getThumbnail(OutputStream outputStream) throws IOException {
        BufferedImage imageThumbnail = mapProvider.getImageThumbnail();
        ImageIO.write(imageThumbnail, "png", outputStream);
    }

    @GetMapping(path = "/maps")
    public void getMapFragment(@RequestParam(value = "leftTopAttitude") double leftTopAttitude,
                               @RequestParam(value = "leftTopLongitude") double leftTopLongitude,
                               @RequestParam(value = "rightDownAttitude") double rightDownAttitude,
                               @RequestParam(value = "rightDownLongitude") double rightDownLongitude,
                               OutputStream outputStream) throws IOException {
        BufferedImage mapFragment = mapProvider.getMapFragment(leftTopAttitude, leftTopLongitude, rightDownAttitude, rightDownLongitude);
        ImageIO.write(mapFragment, "png", outputStream);
    }
}
