package pl.gda.pg.map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;

@Component
public class ImageResizer {

    public BufferedImage getScaledImage(BufferedImage image, int width, int height) {
        BufferedImage bufferedImage = new BufferedImage(width, height, image.getType());
        Graphics2D g = bufferedImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return bufferedImage;
    }
}
