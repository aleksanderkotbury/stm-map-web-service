package pl.gda.pg.map;

import org.springframework.stereotype.Component;
import pl.gda.pg.eti.stm.map.Coordinate;

@Component
public class MapCornersValidator {

    public void validateCorners(Coordinate leftTopCorner, Coordinate rightDownCorner) {
        validateCorner(leftTopCorner);
        validateCorner(rightDownCorner);
    }

    private void validateCorner(Coordinate corner) {
        double leftTopCornerAttitude = corner.getAttitude().doubleValue();
        double leftTopCornerLongitude = corner.getLongitude().doubleValue();
        assert leftTopCornerAttitude <= MapCoordinates.MAX_ATTITUDE && leftTopCornerAttitude >= MapCoordinates.MIN_ATTITUDE;
        assert leftTopCornerAttitude >= MapCoordinates.MIN_LONGITUDE && leftTopCornerAttitude <= MapCoordinates.MAX_LONGITUDE;
    }
}
