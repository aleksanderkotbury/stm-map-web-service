package pl.gda.pg.map;

import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

@Component
public class ImageToBase64Converter {

    public String getBase64(BufferedImage bufferedImage) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        OutputStream base64Image = Base64.getEncoder().wrap(os);
        writeImage(bufferedImage, base64Image);

        return getBase64String(os);
    }

    private String getBase64String(ByteArrayOutputStream os) {
        try {
            return os.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Cannot convert image to base64");
        }
    }

    private void writeImage(BufferedImage bufferedImage, OutputStream base64Image) {
        try {
            ImageIO.write(bufferedImage, "jpg", base64Image);
        } catch (IOException e) {
            throw new RuntimeException("Cannot convert image to base64");
        }
    }
}
