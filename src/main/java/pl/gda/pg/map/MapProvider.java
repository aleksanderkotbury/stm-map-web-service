package pl.gda.pg.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import pl.gda.pg.eti.stm.map.Coordinate;

import java.awt.*;
import java.awt.image.BufferedImage;

@Component
public class MapProvider {

    public static final int THUMBNAIL_SIZE = 500;

    private final BufferedImage mapImage;
    private final ImageResizer imageResizer;
    private final MapCornersValidator mapCornersValidator;
    private final CoordinatesToDimensionConverter coordinatesToDimensionConverter;
    private final MapCutter mapCutter;

    @Autowired
    public MapProvider(BufferedImage mapImage, ImageResizer imageResizer, MapCornersValidator mapCornersValidator,
                       CoordinatesToDimensionConverter coordinatesToDimensionConverter, MapCutter mapCutter) {
        this.mapImage = mapImage;
        this.imageResizer = imageResizer;
        this.mapCornersValidator = mapCornersValidator;
        this.coordinatesToDimensionConverter = coordinatesToDimensionConverter;
        this.mapCutter = mapCutter;
    }

    @Cacheable("imageThumbnail")
    public BufferedImage getImageThumbnail() {
        return imageResizer.getScaledImage(mapImage, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
    }

    public BufferedImage getMapFragment(Coordinate leftTopCorner, Coordinate rightDownCorner) {
        mapCornersValidator.validateCorners(leftTopCorner, rightDownCorner);
        Dimension dimension = coordinatesToDimensionConverter.convertToDimension(leftTopCorner, rightDownCorner);
        return mapCutter.cutImage(mapImage, dimension);
    }

    public BufferedImage getMapFragment(double leftTopAttitude, double leftTopLongitude,
                                        double rightDownAttitude, double rightDownLongitude) {
        Dimension dimension = coordinatesToDimensionConverter.convertToDimension(leftTopAttitude, leftTopLongitude,
                rightDownAttitude, rightDownLongitude);

        return mapCutter.cutImage(mapImage, dimension);
    }

}
