package pl.gda.pg.map;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;

@Component
public class MapCutter {

    public BufferedImage cutImage(BufferedImage image, Dimension dimension) {
        return image.getSubimage(dimension.getX(), dimension.getY(), dimension.getWidth(), dimension.getHeight());
    }
}
