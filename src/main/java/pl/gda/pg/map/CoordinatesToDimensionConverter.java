package pl.gda.pg.map;

import org.springframework.stereotype.Component;
import pl.gda.pg.eti.stm.map.Coordinate;

import javax.annotation.PostConstruct;


@Component
public class CoordinatesToDimensionConverter {

    private double attitudeMapDelta;
    private double longitudeMapDelta;

    @PostConstruct
    public void init() {
        longitudeMapDelta = MapCoordinates.MAX_LONGITUDE - MapCoordinates.MIN_LONGITUDE;
        attitudeMapDelta = MapCoordinates.MAX_ATTITUDE - MapCoordinates.MIN_ATTITUDE;
    }

    public Dimension convertToDimension(Coordinate leftTopCorner, Coordinate rightDownCorner) {
        return convertToDimension(leftTopCorner.getAttitude().doubleValue(), leftTopCorner.getLongitude().doubleValue(),
                rightDownCorner.getAttitude().doubleValue(), rightDownCorner.getLongitude().doubleValue());
    }

    public Dimension convertToDimension(double leftTopAttitude, double leftTopLongitude,
                                        double rightDownAttitude, double rightDownLongitude) {
        int x = getXCoordinate(leftTopAttitude);
        int x2 = getXCoordinate(rightDownAttitude);
        int width = x2 - x;

        int y = getYCoordinate(leftTopLongitude);
        int y2 = getYCoordinate(rightDownLongitude);
        int height = y2 - y;

        return new Dimension(x, y, width, height);

    }

    private int getXCoordinate(double cornerAttitude) {
        double attitudeDelta = cornerAttitude - MapCoordinates.MIN_ATTITUDE;
        return (int) (attitudeDelta * MapCoordinates.MAP_SIZE / attitudeMapDelta);
    }

    private int getYCoordinate(double cornerLongitude) {
        double longitudeDelta = MapCoordinates.MAX_LONGITUDE - cornerLongitude;
        return (int) (longitudeDelta * MapCoordinates.MAP_SIZE / longitudeMapDelta);
    }
}
