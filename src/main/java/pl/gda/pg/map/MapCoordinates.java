package pl.gda.pg.map;

public class MapCoordinates {

    public static final double MIN_LONGITUDE = 54.34674;
    public static final double MAX_LONGITUDE = 54.35744;
    public static final double MIN_ATTITUDE = 18.63400;
    public static final double MAX_ATTITUDE = 18.65255;
    public static final int MAP_SIZE = 942;

    private MapCoordinates() {
    }
}
